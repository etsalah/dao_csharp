﻿/*
 * Created by SharpDevelop.
 * User: etsala
 * Date: 12/16/2013
 * Time: 5:02 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Data;
using System.Collections.Generic;

namespace Doa
{
	/// <summary>
	/// Description of Config.
	/// </summary>
	public sealed class Config
	{
		private static Config instance = new Config("config.xml");
		
		private Dictionary<string,Dictionary<string,string>> _Databases;
		private int _RecordCount;
		private bool _RunInTestMode;
		private string _LevelToCopyFrom;
		
		public int RecordCount {
			get{
				return _RecordCount;
			}
		}
		
		public bool RunInTestMode {
			get{
				return _RunInTestMode;
			}
		}
		public string LevelToCopyFrom {
			get{
				return _LevelToCopyFrom;
			}
		}
		
		public Dictionary<string,string> GetDatabase(string DbToConnectIndex){
			return _Databases[DbToConnectIndex];
		}
		
		public static Config Instance {
			get {
				return instance;
			}
		}
		
		private Config(string PathToDBConfigFile)
		{
			_RecordCount = 100;
			_RunInTestMode = true;
			_LevelToCopyFrom = "INCOMING_SMS";
			_Databases = new Dictionary<string, Dictionary<string, string>>();

            XMLConnReader connReader = new XMLConnReader(PathToDBConfigFile);
            List<XMLConnReader> conns = connReader.GetConnections();
            if (conns != null && conns.Count > 0)
            {
                foreach (XMLConnReader conn in conns)
                {
                    Dictionary<string, string> DatabaseCredentials = new Dictionary<string, string>();
                    DatabaseCredentials.Add("username", conn.Username);
                    DatabaseCredentials.Add("password", conn.Password);
                    DatabaseCredentials.Add("host", conn.Host);
                    DatabaseCredentials.Add("database", conn.Database);
                    _Databases.Add(conn.ConnectionName, DatabaseCredentials);
                }
            }
            ////Database Configuration for Local database
            //Database.Add("username", "root");
            //Database.Add("password", "Dreamfinmng123?");
            //Database.Add("host","localhost");
            //Database.Add("database", "hyperdb");
            //_Databases.Add(0,Database);

            ////Database Configuration for Remote database
            //Database = new Dictionary<string, string>();
            //Database.Add("username", "root");
            //Database.Add("password","Dreamfinmng123?");
            //Database.Add("host","192.168.5.8");
            //Database.Add("database","hyperdb");
            //_Databases.Add(1,Database);

            ////Database Configuration for logger
            //Database = new Dictionary<string, string>();
            //Database.Add("username", "root");
            //Database.Add("password", "Dreamfinmng123?");
            //Database.Add("host", "localhost");
            //Database.Add("database", "smsextrasdb");
            //_Databases.Add(2, Database);

            //Database = new Dictionary<string, string>();
            //Database.Add("username", "root");
            //Database.Add("password", "Dreamfinmng123?");
            //Database.Add("host", "localhost");
            //Database.Add("database", "dflbiometric");
            //_Databases.Add(3, Database);
		}
	}
}
