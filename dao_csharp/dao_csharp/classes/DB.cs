﻿/*
 * Created by SharpDevelop.
 * User: etsala
 * Date: 12/16/2013
 * Time: 12:51 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */

using System;
using System.Data;
using System.Collections.Generic;
using MySql.Data.MySqlClient;

namespace Doa
{
	/// <summary>
	/// Description of DB.
	/// </summary>
	public static class Db
	{

		/***
		 * This method is used by the querying method in this
		 * class to get a connection object to be able to run the queries
		 * against the database
		 */ 
		private static MySqlConnection GetConnectionObject(string DbToConnectTo){
			Config config = Config.Instance;
			Dictionary<string,string> DbConfig = config.GetDatabase(DbToConnectTo);
			string connectionString = string.Format("SERVER={0};DATABASE={1};UID={2};PASSWORD={3}",
			                                        DbConfig["host"], DbConfig["database"] ,DbConfig["username"],
			                                        DbConfig["password"]);
			return new MySqlConnection(connectionString);
		}

		/***
		 * This method returns a boolean indicating if the sql query that
		 * was passed to it was successful
		 * This is not transaction safe
		 */
		public static bool ExecuteNonQuery(string Query, string DbToConnectTo, 
                Dictionary<String,String> Parameters = null){

			MySqlConnection conn = null;
			MySqlCommand cmd = null;
			bool result = false;
			try{
				conn = Db.GetConnectionObject(DbToConnectTo);
				conn.Open();
				cmd = new MySqlCommand(Query, conn);
				if(Parameters != null && Parameters.Keys.Count > 0){
					foreach(String key in Parameters.Keys){
						cmd.Parameters.AddWithValue(key, Parameters[key]);
					}
				}
				result = cmd.ExecuteNonQuery() > 0;
			}catch(MySqlException ex){
				Console.WriteLine("mysql error");
				Console.Write(ex.ToString());
			}catch(Exception ex){
				Console.Write(ex.ToString());
			}finally{
				if(cmd != null){
					cmd.Dispose();
				}
				if(conn != null){
					conn.Close();
				}
			}
			return result;
		}

		/**
		 * This method returns a list of rows returned by executing the sql query 
		 * that is passed to it in a database. It return a null object if nothing
		 * is found
		 */
		public static DataTable ExecuteQuery(string Query, string DbToConnectTo, 
                Dictionary<string,string> Parameters = null){
			MySqlConnection conn = null;
			MySqlCommand cmd = null;
			MySqlDataAdapter da = null;
			DataTable dt = null;
			try{
				conn = Db.GetConnectionObject(DbToConnectTo);
				//conn.Open();
				cmd = new MySqlCommand(Query,conn);
				if(Parameters != null && Parameters.Keys.Count > 0){
					foreach(string key in Parameters.Keys){
						cmd.Parameters.AddWithValue(key,Parameters[key]);
					}
				}
				da = new MySqlDataAdapter(cmd);
				dt = new DataTable();
				da.Fill(dt);
			}catch(MySqlException ex){
				Console.WriteLine(ex.ToString());
			}catch(Exception ex){
				Console.WriteLine(ex.ToString());
			}finally{
				if(da != null){
					da.Dispose();
				}
				if(cmd != null){
					cmd.Dispose();
				}
				if(conn != null){
					conn.Close();
				}
			}
			return dt;
		}
	}
}