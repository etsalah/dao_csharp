﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace Doa
{
    class XMLConnReader
    {

        public string ConnectionName { set; get; }
        public string Username { set; get; }
        public string Password { set; get; }
        public string Host { set; get; }
        public string Database { set; get; }

        private DataTable dt = null;

        public XMLConnReader()
        {
            ConnectionName = null;
            Username = null;
            Password = null;

        }

        public XMLConnReader(string Path)
        {
            try
            {
                DataSet ds = new DataSet();
                ds.ReadXml(Path);
                dt = ds.Tables["server"];
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        public List<XMLConnReader> GetConnections()
        {
            List<XMLConnReader> conns = null;

            if (dt != null && dt.Rows.Count > 0)
            {
                conns = new List<XMLConnReader>();
                foreach (DataRow row in dt.Rows)
                {
                    conns.Add(new XMLConnReader
                    {
                        ConnectionName = row.Field<string>("name"),
                        Password = row.Field<string>("password"),
                        Username = row.Field<string>("username"),
                        Host = row.Field<string>("host"),
                        Database = row.Field<string>("database")
                    });
                }
            }

            return conns;
        }

    }
}
